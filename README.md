# Purpose

This is an empty package that doesn't really do anything. It's just a test for
practice publishing to test.pypi.org (and eventually to pypi.org for other,
actual projects)

## What does this package actually do?

Nothing! Do not download or use it